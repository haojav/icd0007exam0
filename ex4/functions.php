<?php

function standardDeviation($inputList) {
    $count = count($inputList);
    $average = array_sum($inputList) / $count;

    $variance = 0.0;
    foreach($inputList as $each) {
        $variance += pow($each - $average, 2);
    }

    return round(sqrt($variance / $count), 2);
}

function getConnectionWithData($dataFile) {
    $conn = new PDO('sqlite::memory:');
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $statements = explode(';', join('', file($dataFile)));

    foreach ($statements as $statement) {
        $conn->prepare($statement)->execute();
    }

    return $conn;
}
